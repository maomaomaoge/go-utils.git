[TOC]

# go-utils

#### 介绍
go平时用的的api

#### 正在处理

磁盘读写要设置参数, 文件大小，或者一次最大缓冲区

#### 使用姿势

##### 1简单文件服务器

```
server端: go-utils sever port

client: go-utils  easyFtp 47.99.114.150:5213 ./testfiles/sim
```



##### 2内外网传输数据

```
server(外网): go-utils keepServe

client(内网): go-utils KeepClient 47.99.114.150:5213

下面进行命令行交互
```



#### 常用命令



##### linux

1.  io	speed

   > 获取磁盘读写速度



#### 版本
1. v2.0.0

   ```
   1. 
   linux添加临时环境变量
   linux让临时环境变量生效
   主要是临时补全使用
   
   2.
   增加version
   ```

   

2. v1.0.7

   ```
   windows支持文档生成读取，自己找gopath下面的文档
   
   配置go_path的bin的系统环境变量，执行 go-utils
   ```

3. v1.0.9

   ```
   无锁队列，没实现
   ```

4. v1.1.0

   ```
   1 增加函数毫秒时间戳函数
   2 增加lz4算法
   ```

5. v1.2.0

   ```
   2022-07-11
   
   1 文档变为html
   	在运行bin的可执行文件时，就是把那个html拷贝大当前文件夹下，或者生成个文件服务器去映射
   ```

6. v1.2.1

   ```
   1 加入cobra
   linux命令限制
   	--help 
   		
   	rm -rf .
       
   2 文件上传
   ```

7. v1.2.3

   ```
   1 简单的linux文件上传
   ```

8. v1.2.5

   ```
   双向数据流
   一般为外网访问不到我们的内网数据
   外网tcp服务器
   内网订阅
   
   内网可以命令行继续交互
   ```



aa