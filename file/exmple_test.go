package file

import "fmt"

func ExamplePathExists() {
	var (
		readMe string
		exists bool
		err error
	)

	// 存在文件形式
	readMe = "./README.md"
	if exists, err = PathExists(readMe); err != nil {
		return
	}
	fmt.Println(exists)
	readMe = "./log/README.md"
	if exists, err = PathExists(readMe); err != nil {
		return
	}
	fmt.Println(exists)

	// 不存在文件，只是单纯的目录
	readMe = "./"
	if exists, err = PathExists(readMe); err != nil {
		return
	}
	fmt.Println(exists)
	readMe = "./log"
	if exists, err = PathExists(readMe); err != nil {
		return
	}
	fmt.Println(exists)

	// 不存在的文件
	readMe = "./l.log"
	if exists, err = PathExists(readMe); err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(exists)

	// Output:
	// true
	// false
	// true
	// false
	// false
}
