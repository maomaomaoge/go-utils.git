package file

import (
	"fmt"
	"testing"
)

func TestReadTextWithoutNT(t *testing.T) {
	nt, err := ReadTextWithoutNT("../testfiles/testgo.txt")
	if err != nil {
		return
	}

	fmt.Println(nt)
}
