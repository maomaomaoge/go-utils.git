package file

import (
	"os"
	"text/scanner"
)

// func: ReadTextWithoutNT(fp string) (string, error)
// aim: 读取txet去除空格和换行,和空格

func ReadTextWithoutNT(fp string) (string, error) {
	var s scanner.Scanner
	open, err := os.Open(fp)
	if err != nil {
		return "", err
	}
	s.Init(open)
	s.Whitespace ^= 1<<'\t' | 1<<'\n' | 1 << ' ' // don't skip tabs and new lines

	result := ""
	for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {
		if tok == '\n' || tok == '\t' {
			continue
		}

		result += s.TokenText()
	}
	
	return result, nil
}
