package file

import (
	"fmt"
	"testing"
)

func TestCreateDir(t *testing.T) {
	err := CreateDir("1", "2", "3")
	if err != nil {
		fmt.Println(err)
		return
	}

	err = CreateDir("./one/1", "./two/2", "./three/3")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("crete dir ok")
}

