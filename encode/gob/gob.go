package gob

import (
	"bytes"
	"encoding/gob"
)

// func: Encode(data interface{}) ([]byte, error)
// aim: gob方式把数据变为[]byte

func Encode(data interface{}) ([]byte, error) {
	var network bytes.Buffer        // Stand-in for a network connection
	enc := gob.NewEncoder(&network) // Will write to network.
	err := enc.Encode(data)
	if err != nil {
		return nil, err
	}

	return network.Bytes(), nil
}
