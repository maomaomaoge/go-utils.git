package math

import "gitee.com/maomaomaoge/go-utils/shift"

// func: FetchUnitsDigit() (int, error)
// aim: 取出个位数字

func FetchUnitsDigit(data interface{}) (int, error) {
	toInt64, err := shift.ToInt64(data)
	if err != nil {
		return 0, err
	}

	res := toInt64 % 10
	return int(res), nil
}
