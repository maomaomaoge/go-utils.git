package math

import (
	"testing"
)

func TestFetchUnitsDigit(t *testing.T) {
	t.Run("测试1", func(t *testing.T) {

		numbers := 3457.1189

		digit, err := FetchUnitsDigit(numbers)
		if err != nil {
			t.Fatal(err)
		}

		want := 7
		got := digit

		if want != got {
			t.Errorf("got %d want %d given, %v", got, want, numbers)
		}
	})

	t.Run("测试2", func(t *testing.T) {
		numbers := 33.123

		digit, err := FetchUnitsDigit(numbers)
		if err != nil {
			t.Fatal(err)
		}

		want := 7
		got := digit

		if want != got {
			t.Errorf("got %d want %d given, %v", got, want, numbers)
		}
	})

	t.Run("测试3", func(t *testing.T) {
		numbers := 892

		digit, err := FetchUnitsDigit(numbers)
		if err != nil {
			t.Fatal(err)
		}

		want := 7
		got := digit

		if want != got {
			t.Errorf("got %d want %d given, %v", got, want, numbers)
		}
	})
}

func BenchmarkFetchUnitsDigit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FetchUnitsDigit(4.4441)
	}
}