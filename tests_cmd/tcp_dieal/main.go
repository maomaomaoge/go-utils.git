package main

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"time"
)

func main() {
	conn, err := net.DialTimeout("tcp", "127.0.0.1:7000", time.Second * 2)
	if err != nil {
		return
	}

	go clientRead(conn)

	for {
		write(conn)
		time.Sleep(time.Second * 1)
	}
}

func write(r io.Writer)  {
	buffer := bytes.NewBuffer([]byte{1, 2, 3})
	_, err := buffer.WriteTo(r)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func clientRead(conn net.Conn)  {
	for {
		data := make([]byte, 1024)
		read, err := conn.Read(data)
		if err != nil {
			return
		}
		fmt.Println("client  接受 server 字节", read)
	}
}