package buff

import (
	"fmt"
	"testing"
)

func getBuf() *Buffer {
	buffer := NewBuffer(30)

	return buffer
}

func TestBuffer_Byte(t *testing.T) {
	buf := getBuf()

	buf.PutByte(byte(1))

	var hook Hook = func(buf *Buffer) {
		fmt.Println(buf.pos)
		fmt.Println(buf.Buf)
	}

	buf.PutHook(hook)
}

func TestBuffer_PutUint16(t *testing.T) {
	buf := getBuf()

	buf.PutUint16(uint16(32769))

	var hook Hook = func(buf *Buffer) {
		fmt.Println(buf.pos)
		fmt.Println(buf.Buf)
	}

	buf.PutHook(hook)
}

func TestBuffer_PutUint32(t *testing.T) {
	buf := getBuf()

	buf.PutUint32(uint32(259))

	var hook Hook = func(buf *Buffer) {
		fmt.Println(buf.pos)
		fmt.Println(buf.Buf)
	}

	buf.PutHook(hook)
}

func TestBuffer_Float(t *testing.T) {
	buf := getBuf()

	buf.PutFloat(float32(259))

	var hook Hook = func(buf *Buffer) {
		fmt.Println(buf.pos)
		fmt.Println(buf.Buf)
	}

	buf.PutHook(hook)
}

type person struct {
	Id int32
	Av float64
}

func TestBuffer_Hex(t *testing.T) {
	p := &person{
		Id: 11,
		Av: float64(22.12212),
	}

	buf := getBuf()

	fmt.Println("encode")
	buf.PutUint32(uint32(p.Id))
	var hook Hook = func(buf *Buffer) {
		fmt.Println(buf.pos)
	}
	buf.PutHook(hook)

	buf.PutFloat64(float64(p.Av))
	var hook2 Hook = func(buf *Buffer) {
		fmt.Println(buf.pos)
	}
	buf.PutHook(hook2)

	fmt.Println("decode")
	value := buf.Uint32()
	fmt.Println(value)
	f := buf.Float64()
	fmt.Println(f)
}