module gitee.com/maomaomaoge/go-utils

go 1.16

require (
	github.com/bkaradzic/go-lz4 v1.0.0
	github.com/frankban/quicktest v1.13.0 // indirect
	github.com/gin-gonic/gin v1.7.2
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/mitchellh/mapstructure v1.4.1
	github.com/pierrec/lz4 v2.6.1+incompatible
	github.com/shirou/gopsutil/v3 v3.21.6
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	github.com/things-go/go-modbus v0.0.0-20210722035225-656ba9dd1caa
	github.com/tmc/scp v0.0.0-20170824174625-f7b48647feef
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
