package scp

import (
	"fmt"
	"golang.org/x/crypto/ssh"
	"testing"
	"time"
)

func TestCopyFile(t *testing.T) {
	var auths []ssh.AuthMethod
	auths = append(auths, ssh.Password("Maomao123456"))
	config := ssh.ClientConfig{
		Timeout:         time.Second * 30,
		User:            "root",
		Auth:            auths,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	//创建tcp连接
	conn, err := ssh.Dial("tcp", "47.99.114.150:22", &config)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()

	fmt.Println("连接ok")
}
