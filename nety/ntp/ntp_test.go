package ntp

import (
	"fmt"
	"testing"
)

func TestNtp(t *testing.T) {
	tm, err := GetNetworkTime("0.pool.ntp.org:123")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(tm.String())
}
