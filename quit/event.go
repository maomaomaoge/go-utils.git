package quit

import (
	"context"
	"sync"
)

var q *quit

type quit struct {
	Ctx    context.Context
	Cancel context.CancelFunc
	Wg     *sync.WaitGroup
}

func init() {
	q = &quit{
		Wg: new(sync.WaitGroup),
	}
	q.Ctx, q.Cancel = context.WithCancel(context.Background())
}

//获取退出context
func Over() context.Context {
	return q.Ctx
}

//中断
func Cancel() {
	q.Cancel()
}

func AddOne() {
	q.Wg.Add(1)
}

func DoneOne() {
	q.Wg.Done()
}

func Wait() {
	q.Wg.Wait()
}


