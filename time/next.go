package time

import "time"

// func: NextIntSecond() time.Duration
// aim: 下一个整数秒的时间间隔

func NextIntSecond() time.Duration {
	// 计算下一秒的时间
	now := time.Now()
	_1s, _ := time.ParseDuration("+1s")
	nextSecond := now.Add(_1s)

	// 取出下一秒时间的整数秒
	nextRound := time.Date(nextSecond.Year(), nextSecond.Month(), nextSecond.Day(), nextSecond.Hour(), nextSecond.Minute(), nextSecond.Second(), 0, time.Local)

	// 计算时间差
	diff := nextRound.Sub(now)

	return diff
}

