package time

import (
	"fmt"
	"strings"
	"testing"
	"time"
)

func TestTimeUnixToString(t *testing.T) {
	toString := TimeUnixToString(time.Now().Unix())
	fmt.Println(toString)
	fmt.Println(strings.ReplaceAll(toString, " ", "-"))
}
