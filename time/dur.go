// 时间间隔包
package time

import "time"

// func: Today24Dur() (time.Duration, error)
// aim: 获取今天半夜12点到现在的时间间隔

func Today24Dur() (time.Duration, error) {
	t := time.Now()
	//

	date := t.AddDate(0, 0, 1)
	night := time.Date(date.Year(), date.Month(), date.Day(), 0, 0, 0, 0, time.Local)
	sub := night.Sub(t)

	return sub, nil
}