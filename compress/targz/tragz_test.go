package targz

import (
	"fmt"
	"testing"
)

func TestTarEncode(t *testing.T) {
	err := TarEncode("./GB_Simulate", "GB_Simulate.tar.gz")
	if err != nil {
		fmt.Println(err)
	}
}
