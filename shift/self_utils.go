package shift

import "errors"

func errNegativeNotAllowed() error {
	return errors.New("类型转换失败")
}