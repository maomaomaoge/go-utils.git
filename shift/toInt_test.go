package shift

import (
	"fmt"
)

func ExampleToInt() {
	res1, err := ToInt(1.1)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(res1)

	res2, err := ToInt("2.2")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(res2)

	// Output:
	// 1
	// 2
}