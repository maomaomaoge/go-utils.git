package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version [sim/det]",
	Short: "查看当前版本",
	Long: `
不加参数查看简单的版本
- sim: 查看简单的
- det: 查看复杂的版本信息
`,
	Run: func(cmd *cobra.Command, args []string) {
		sim := `v2.0.0`
		det := `
------------------------------------------------------------------
	gus 2.0.0 [niu bi] build 2021-08-24
	Copyright 2004-2021, Magus Technology Co.,Ltd.
	index: 7
------------------------------------------------------------------
`

		if len(args) == 0 {
			fmt.Println(sim)
			return
		}

		switch args[0] {
		case "sim":
			fmt.Println(sim)
		case "det":
			fmt.Println(det)
		default:
			fmt.Println("没有这个命令")
		}
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
