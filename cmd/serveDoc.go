/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"

	"github.com/spf13/cobra"
)

// serveDocCmd represents the serveDoc command
var serveDocCmd = &cobra.Command{
	Use:   "serveDoc [port]",
	Short: "在线文档",
	Long: `在线文档
路由 /doc
默认端口: 12345`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			serve("12345")
		} else {
			serve(args[0])
		}
	},
}

func init() {
	rootCmd.AddCommand(serveDocCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serveDocCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serveDocCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func serve(port string)  {
	router := gin.Default()
	fmt.Println("文档地址；", HtmlFile)
	router.StaticFS("/doc", http.Dir(HtmlFile))
	router.Run(":" + port)
}