// 数据协议, 字符串的字节数(4字节) + 文件字符串消息 + 内容长度 4字节 +  数据
package cmd

import (
	"fmt"
	"gitee.com/maomaomaoge/go-utils/buff"
	"github.com/spf13/cobra"
	"io/ioutil"
	"log"
	"net"
)

// easyFtpCmd represents the easyFtp command
var easyFtpCmd = &cobra.Command{
	Use:   "easyFtp [tcp_addr] [filepath]",
	Short: "简单的ftp",
	Long: `简单的ftp, 如果参数`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Fatalln("参数不正确")
		}
		file, err := ioutil.ReadFile(args[1])
		if err != nil {
			log.Fatalln("文件打开失败", err)
		}
		bytes := pack(args[1], file)

		ftp(args[0], bytes)
	},
}

func init() {
	rootCmd.AddCommand(easyFtpCmd)
}

func ftp(addr string, data []byte)  {
	var d net.Dialer

	conn, err := d.Dial("tcp", addr)
	if err != nil {
		log.Fatalf("Failed to dial: %v", err)
	}
	defer conn.Close()

	l, err := conn.Write(data)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("发送字节数: ", l)
}

func pack(fileName string, data []byte) []byte {
	if fileName == "" {
		log.Fatalln("文件名为空")
	}

	f := []byte(fileName)
	head := len(f) // 文件名字字符串的消息长度头

	size := 4 + 4 + len(f) + len(data) // 总消息长度: 消息总字节数 + 文件名字头 + 文件名字内容 + 文件内容

	buffer := buff.NewBuffer(size)

	//设置文件总大小消息头
	buffer.PutInt(len(data))

	//  设置文件名字消息头
	buffer.PutInt(head)

	//  设置文件名字
	buffer.PutArray(f)
	fmt.Println("文件名字节数: ", len(f))

	//  设置文件
	buffer.PutArray(data)
	fmt.Println("pack的文件内容字节数: ", len(data))
	return buffer.Buf
}
